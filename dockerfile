FROM php:7.1.3-apache


RUN echo "deb [check-valid-until=no]  http://ftp.us.debian.org/debian/ jessie main" > /etc/apt/sources.list.d/jessie-backports.list
RUN sed -i '/deb http:\/\/deb.debian.org\/debian jessie-updates main/d' /etc/apt/sources.list
RUN apt-get -o Acquire::Check-Valid-Until=false update


RUN apt-get update \
    && apt-get install -y --no-install-recommends git zip unzip zlib1g-dev libmcrypt-dev libpq-dev wget libpng-dev \
    && docker-php-ext-install mysqli pdo_mysql zip json\
    && a2enmod rewrite

# Composer install
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
